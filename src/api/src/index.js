/**
 *––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
 * # semantyk.com
 * Module | `index.js`
 *
 * June 15, 2022
 *
 * Copyright © Semantyk 2022. All rights reserved.
 –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
 */

//* External Imports
const express = require('express')

//* Main
const app = express()
const port = process.env.PORT || 3030

app.get('/', (req, res) => {
  res.send(`API running on port ${port}`)
})

app.listen(port, err => {
  if (err) console.log(`Error: ${err}`)
  else console.log(`API running on port ${port}`)
})