/**
 * –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
 * # semantyk.com
 * Module | `MainHead.tsx`
 *
 * June 13, 2022
 *
 * Copyright © Semantyk 2022. All rights reserved.
 * –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
 */

//* External Imports
import {Component} from 'react';
import {Helmet} from 'react-helmet-async';

//* Main
export default class MainHead extends Component<{ description?: string, preview?: string, title?: string, url?: string }, {}> {
    render() {
        const title = this.props.title === undefined ? 'Semantyk' : this.props.title;
        const description = this.props.description === undefined ? 'Ideas Wonder. |' +
            ' Visual Interactive Semantic Intelligent Online Network.' : this.props.description;
        const preview = this.props.preview === undefined ? '/preview.png' : this.props.preview;
        const apple_icon = this.props.preview === undefined ? '/apple-touch-icon.png' : this.props.preview;
        const domain = this.props.url === undefined ? 'https://www.semantyk.com' : this.props.url;
        const favicon = this.props.preview === undefined ? '/favicon.ico' : this.props.preview;
        const keywords = "Semantyk, Linked Data, Solid, CSS, JavaScript";
        const url = this.props.url === undefined ? window.location.href : this.props.url;
        return (
            <Helmet>
                {/* Primary Meta Tags */}
                <link rel="icon" href={favicon}/>
                <title>{title}</title>
                <meta charSet="utf-8"/>
                <meta name="title" content={title}/>
                <meta name="description" content={description}/>
                <meta name="keywords" content={keywords}/>
                <meta name="viewport"
                      content="width=device-width, initial-scale=1"/>
                {/* Apple Tags */}
                <link rel="apple-touch-icon" href={apple_icon}/>
                {/* Open Graph / Facebook Meta Tags */}
                <meta name="facebook-domain-verification"
                      content="787eo0v092pbygw2kjh68w1gfqu28p"/>
                <meta property="og:description" content={description}/>
                <meta property="og:image" content={preview}/>
                <meta property="og:title" content={title}/>
                <meta property="og:type" content="website"/>
                <meta property="og:url" content={url}/>
                {/* Twitter Meta Tags */}
                <meta property="twitter:card" content="summary_large_image"/>
                <meta property="twitter:description" content={description}/>
                <meta property="twitter:domain" content={domain}/>
                <meta property="twitter:image" content={preview}/>
                <meta property="twitter:url" content={url}/>
                <meta property="twitter:title" content={title}/>
            </Helmet>
        );
    }
}